import React, { Component } from 'react';
import Cell from './Cell';

export default class Board extends Component {

  renderRow(i) {
    const size = this.props.size;
    let cols = Array.from({length: size}, (x, i) => i);
    return cols.map((j) => {
      let cell = i * size + j;
      return (
      <Cell
        key={j}
        cell={cell}
        value={this.props.squares[cell]}
        lost={this.props.lost}
        onClick={() => this.props.onClick(cell)}
        onContextMenu={(e) => {e.preventDefault(); this.props.onContextMenu(cell)}} />
    )});
  }
  render() {
    let row = Array.from({length: this.props.size}, (x, i) => i);
    return (
      <div className={this.props.bigness}>
        {row.map((i) =>( <div className="boardRow" key={i}>{this.renderRow(i)}</div> ))}
      </div>
    );
  }
}

