import React, { Component } from 'react';
import { formatTimeSpan } from './helpers';

export default class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      elapsed: 0
    };
    let tick = this.tick.bind(this);
    this.interval = setInterval(tick, 1000);
  }

  tick() {
    const started = this.props.started;
    if (started && this.props.lost === false && this.props.won === false) {
      const elapsed = Date.now() - started;
      this.setState({elapsed});
    }
  }
    
  elapsed() {
    return formatTimeSpan(this.state.elapsed);
  }

  render() {
    const elapsed = this.elapsed();
    const label = "Elapsed Time";
    return (
      <input disabled value={elapsed} title={label} placeholder={label} />
    );
  }
}
