import React, { Component } from 'react';
import './App.scss';
import Board from './Board';
import Counter from './Counter';
import Settings from './Settings';
import Records from './Records';
import Help from './Help';
import Header from './Header';
import Footer from './Footer';

import { cellNeighbors, makeRange, shuffle, sizes, mineCounts, formatTimeSpan } from './helpers';
import { default_bigness, default_difficulty, unrevealed, flag, bomb, flaggedBomb, before, during, won, lost } from './consts';

class App extends Component {
  constructor(props) {
    super(props);

    const size = sizes(default_bigness);
    const mineCount = mineCounts(default_bigness, default_difficulty);
    const n = size * size;

    console.log(mineCount);
    if (mineCount >= n) {
      console.error("More Mines than cells");
      return;
    }

    const mines = shuffle(makeRange(n)).splice(0, mineCount);

    this.state = {
      squares: Array(size * size).fill(" "),
      mines: mines,
      lost: false,
      won: false,
      started: false,
      flagCount: 0,
      bigness: default_bigness,
      difficulty: default_difficulty,
      size: size,
      mineCount: mineCount
    };

  }

  componentDidMount() {
    document.title = "Minesweeper";
  }

  reset(bigness, difficulty) {

    const size = sizes(bigness);
    const mineCount = mineCounts(bigness, difficulty);
    
    const n = size * size;
    const squares = Array(n).fill(unrevealed);
    const mines = shuffle(makeRange(n)).splice(0, mineCount);
    const lost = false;
    const won = false;
    const started = false;
    const flagCount = 0;
    

    this.setState({
      squares,
      mines,
      lost,
      won,
      started,
      flagCount,
      bigness,
      difficulty,
      size,
      mineCount
    });
  }

  value(cell) {
    let found = 0;
    const size = this.state.size;
    const mines = this.state.mines;
    const neighbors = cellNeighbors(cell, size);
    neighbors.forEach(function(neighbor) {
      if (mines.includes(neighbor)) {
        found += 1;
      }
    });
    return found;
  }

  reveal(cell) {
    const size = this.state.size;

    let neighbors = cellNeighbors(cell, size);

    const mines = this.state.mines;
    const squares = this.state.squares;

    //guards
    if (this.state.lost) {
      return;
    }
    if (squares[cell] !== unrevealed) {
      return;
    }
    if (mines.includes(cell)) {
      return this.blow(cell);
    }

    const found = this.value(cell);
    
    squares[cell] = found;
    this.update(squares);

    //recurse on zero
    if (found === 0) {
      neighbors.forEach(neighbor => this.reveal(neighbor));
    }

    this.checkWon();
  }

  flag(cell) {
    //guards
    if (this.state.lost) {
      return;
    }
    let squares = this.state.squares;
    if (squares[cell] !== unrevealed && squares[cell] !== flag) {
      return;
    }

    //toggle flag
    if (squares[cell] === flag) {
      squares[cell] = unrevealed;
    } else {
      squares[cell] = flag;
    }
    this.update(squares);
  }

  update(squares) {
    let started = this.state.started;
    let flagCount = 0;
    if (!started) {
      started = Date.now();
    }
    squares.forEach(function (value) {
      if (value === flag) {
        flagCount += 1;
      }
    });

    this.setState({squares, started, flagCount});
  }

  handleRightClick(cell) {
    if (this.over()) {
      return;
    }
    this.flag(cell);
  }

  handleClick(cell) {
    if (this.over()) {
      return;
    }
    const size = this.state.size;
    const squares = this.state.squares;
    if (squares[cell] === unrevealed ) {
      this.reveal(cell);
    } else {
      const neighbors = cellNeighbors(cell, size);
      let flags = 0;
      neighbors.forEach(function(neighbor) {
        if (squares[neighbor] === flag) {
          flags += 1;
        }
      });
      if ( flags >= this.value(cell)) {
        neighbors.forEach(neighbor => {
          if (squares[neighbor] !== flag) {
            this.reveal(neighbor);
          }
        });
      }
    }
  }

  blow(cell) {
    const squares = this.state.squares;
    const mines = this.state.mines;
    const lost = cell;

    squares[cell] = bomb;
    mines.forEach(function (mine) {
      if (squares[mine] === flag) {
        squares[mine] = flaggedBomb;
      } else {
        squares[mine] = bomb;
      }
    });

    this.setState({squares, lost});
  }

  checkWon() {
    const squares = this.state.squares;
    const mines = this.state.mines;

    let won = squares.every(function (value, cell) {
      return mines.includes(cell) || value !== unrevealed;
    });

    if (won) {
      this.updateLog();
    }
    

    this.setState({won});
  }

  over() {
    return (this.state.lost || this.state.won);
  }

  updateLog() {
    const elapsed = Date.now() - this.state.started;
    const bigness = this.state.bigness;
    const difficulty = this.state.difficulty;
    window.localStorage.setItem(bigness + "-" + difficulty, formatTimeSpan(elapsed));
  }

  render() {
    let status = before;
    let statusDesc = "Reveal a cell to start the game.";
    if (this.state.started) {
      status = during;
      statusDesc = "The clock is ticking.";
      if (this.state.lost !== false) {
        status = lost;
        statusDesc = "You blew up.";
      }
      if (this.state.won) {
        status = won;
        statusDesc = "You won.";
      }
    }
    const unflagged = this.state.mineCount - this.state.flagCount;

    return (
      <div className="container">
        <div className="row">
          <Header />
          <div className="col-sm-8 text-center board">
            <Board 
              size={this.state.size}
              bigness={this.state.bigness}
              squares={this.state.squares} 
              lost={this.state.lost}
              onContextMenu={(i) => this.handleRightClick(i)} 
              onClick={(i) => this.handleClick(i)} />
          </div>
          <div className="col-sm-4">
            <h1 className="text-center" title={statusDesc}>{status}</h1>
            <Counter started={this.state.started} lost={this.state.lost} won={this.state.won} />
            <input className="unflagged" disabled value={unflagged} title="Unflagged Mines" />
            <br />
            <Help />
          </div>
        </div>
        <div className="row">
          <div className="col-sm-6">
            <Records bigness={this.state.bigness} difficulty={this.state.difficulty} />
          </div>
          <div className="col-sm-6">
            <Settings bigness={this.state.bigness} difficulty={this.state.difficulty} onClick={(bigness, difficulty) => this.reset(bigness, difficulty)} />
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <Footer />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
