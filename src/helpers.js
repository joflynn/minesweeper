export function cellNeighbors(cell, size) {
  let t = top(cell, size);
  let r = right(cell, size);
  let b = bottom(cell, size);
  let l = left(cell, size);

  let neighbors = [];
  if (t !== false) {
    neighbors.push(t);

    if (r !== false) {
      neighbors.push(r - size);
    }
  }

  if (r !== false) {
    neighbors.push(r);

    if (b !== false) {
      neighbors.push(b + 1);
    }
  }

  if (b !== false) {
    neighbors.push(b);

    if (l !== false) {
      neighbors.push(l + size);
    }
  }

  if (l !== false) {
    neighbors.push(l);

    if (t !== false) {
      neighbors.push(t - 1);
    }
  }

  //console.log({cell, neighbors});
  return neighbors;
}

function top(cell, size) {
  if (cell >= size) {
    return cell - size;
  }
  return false;
}
function right(cell, size) {
  if ((cell+1) % size !== 0) {
    return cell + 1;
  }
  return false;
}
function bottom(cell, size) {
  if (cell <= (size * (size-1))) {
    return cell + size;
  }
  return false;
}
function left(cell, size) {
  if (cell % size !== 0) {
    return cell - 1;
  }
  return false;
}

export function makeRange(length) {
  let range = [];
  for (let i = 0; i < length; i += 1) {
    range.push(i);
  }
  return range;
}

export function shuffle(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

export function sizes(bigness) {
  let size = 8;
  switch(bigness) {
    case "large":
      size = 20;
      break;
    case "medium":
      size = 12;
      break;
    default:
    case "small":
      size = 8;
      break;
  }

  return size;
}

export function mineCounts(bigness, difficulty) {
  let counts;
  switch(bigness) {
    case "large":
      counts = {easy: 25, medium: 50, hard: 100};
      break;
    case "medium":
      counts = {easy: 10, medium: 15, hard: 20};
      break;
    default:
    case "small":
      counts = {easy: 5, medium: 10, hard: 15};
      break;
  }

  return counts[difficulty];
}

export function formatTimeSpan(ms) {
  const elapsed = ms / 1000;
  let m = Math.floor(elapsed / 60);
  let s = Math.floor(elapsed % 60);
  if (m < 10) {
    m = "0" + m;
  }
  if (s < 10) {
    s = "0" + s;
  }
  return m + ":" + s;
}
