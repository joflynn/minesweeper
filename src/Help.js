import React from 'react';
export default function() {
  return (
    <div className="help">
      <strong>Help</strong>
      <p>
        Left click to reveal a cell.
      </p>
      <p>
        Right click to flag it. On mobile, press and hold.
      </p>
      <p>
        Left click a revealed cell to reveal it's unflagged neighbors.
      </p>
    </div>
  );
}
