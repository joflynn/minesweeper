import React from 'react';
export default function() {
  return (
    <div>
      Written by Joe Flynn in 2018. 
      Source code available at <a href="https://gitlab.com/joflynn/minesweeper">Gitlab</a>.
    </div>
  );
}
