//export const size = 8;
//export const mineCount = 5; 

export const bignesses = ["small", "medium", "large"];
export const difficulties = ["easy", "medium", "hard"];

export const default_bigness = "small";
export const default_difficulty = "medium";

export const unrevealed = " ";
export const blank = "-";
export const flag = "🚩 ";
export const bomb = "💣 ";
export const flaggedBomb = "🎌";

export const before = "😐";
export const during = "😬";
export const won = "😎";
export const lost = "😱";

