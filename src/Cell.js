import React, { Component } from 'react';

import {flag, bomb, unrevealed } from './consts';

export default class Cell extends Component {
  render() {
    let klass;
    switch(this.props.value) {
      case unrevealed:
        klass = "unrevealed";
        break;
      case bomb:
        klass = "bomb";
        if (this.props.lost === this.props.cell) {
          klass += " boom";
        }
        break;
      case flag:
        klass = "flag";
        break;
      default:
        klass = "revealed-" + this.props.value;
        break;
    }
    return (
      <button
        title={this.props.cell}
        className={klass}
        onClick={this.props.onClick}
        onContextMenu={this.props.onContextMenu}>
          {this.props.value}
      </button>
    );
  }
}

