import React, { Component } from 'react';

import { bignesses, difficulties } from './consts';

export default class Settings extends Component {

  render() {
    let bigness_classes = {};
    bignesses.map(label => bigness_classes[label] = this.props.bigness === label ? "selected" : "unselected");

    let difficulty_classes = {};
    difficulties.map(label => difficulty_classes[label] = this.props.difficulty === label ? "selected" : "unselected");
    return (
      <form className="settings">
        <div>
          <strong>Size</strong>
          <div className="button-strip">
            <button onClick={(e) => {e.preventDefault(); this.props.onClick("small", this.props.difficulty)}} className={bigness_classes.small}>Small</button>
            <button onClick={(e) => {e.preventDefault(); this.props.onClick("medium", this.props.difficulty)}} className={bigness_classes.medium}>Medium</button>
            <button onClick={(e) => {e.preventDefault(); this.props.onClick("large", this.props.difficulty)}} className={bigness_classes.large}>Large</button>
          </div>
        </div>
        <div>
          <strong>Difficulty</strong>
          <div>
            <button onClick={(e) => {e.preventDefault(); this.props.onClick(this.props.bigness, "easy")}} className={difficulty_classes.easy}>Easy</button>
            <button onClick={(e) => {e.preventDefault(); this.props.onClick(this.props.bigness, "medium")}} className={difficulty_classes.medium}>Medium</button>
            <button onClick={(e) => {e.preventDefault(); this.props.onClick(this.props.bigness, "hard")}} className={difficulty_classes.hard}>Hard</button>
          </div>
        </div>
        <p>Click a size or difficulty to reset the board.</p>
      </form>
    );
  }
}
