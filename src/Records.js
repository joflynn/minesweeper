import React, { Component } from 'react';

export default class Records extends Component {

  render() {
    const bigness = this.props.bigness;
    const difficulty = this.props.difficulty;

    const record = window.localStorage.getItem(bigness + "-" + difficulty);
    let fastest = "(none yet)";
    if (record) {
      fastest = record;
    }

    return (
      <div>
        <strong>Fastest: </strong>
        {fastest}
      </div>
    );
  }
}
